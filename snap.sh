#!/usr/bin/env bash

set -e
# set -x

APP=$(grep name snapcraft.yaml | cut -d' ' -f 2)
VERSION=$(grep version snapcraft.yaml | cut -d' ' -f 2 | sed 's/"//g')
SNAP_PACKAGE="${APP}_${VERSION}_amd64.snap"

if [[ -e "$SNAP_PACKAGE" ]]
then
    rm "$SNAP_PACKAGE"
    sudo snap remove "${APP}"
fi

# no more than 1 argument
if [ $# -gt 1 ]
then
    echo "Don't be too anxious! one argument per time, please ;) "
    exit 1
fi

# handle argument
case "$1" in

# quit arguments
clean)
    snapcraft clean
    exit 0
    ;;
upload)
    STRICT_MODE=$(grep confinement snapcraft.yaml | grep strict | cut -d' ' -f1)
    if [[ "$STRICT_MODE" == "#" ]]
    then
        echo "uncomment 'confinement: strict' in snapcraft.yml!!!"
        exit 2
    fi
    snapcraft upload --release=stable "$SNAP_PACKAGE"
    exit 0
    ;;

# non-exiting arguments
debug)
    SNAPCRAFT_FLAGS="--debug"
    SNAP_FLAGS="--devmode --dangerous"

    # if 'confinement: devmode' is commented, notify it to the user
    DEV_MODE=$(grep confinement snapcraft.yaml | grep devmode | cut -d' ' -f1)
    if [[ "$DEV_MODE" == "#" ]]
    then
        echo "uncomment 'confinement: devmode' in snapcraft.yml!!!"
        exit 1
    fi

    if ! $(snap list | grep "$SNAP_PACKAGE")
    then
        sudo snap remove "$SNAP_PACKAGE"
    fi
    snapcraft $SNAPCRAFT_FLAGS && sudo snap install $SNAP_FLAGS "$SNAP_PACKAGE"
    ;;
*)  # release build
    SNAPCRAFT_FLAGS=""
    SNAP_FLAGS="--dangerous"

    # if 'confinement: strict' is commented, notify it to the user
    STRICT_MODE=$(grep confinement snapcraft.yaml | grep strict | cut -d' ' -f1)
    if [[ "$STRICT_MODE" == "#" ]]
    then
        echo "uncomment 'confinement: strict' in snapcraft.yml!!!"
        exit 3
    fi

    if $(snap list | grep "$SNAP_PACKAGE")
    then
        sudo snap remove "$SNAP_PACKAGE"
    fi
    snapcraft $SNAPCRAFT_FLAGS && sudo snap install $SNAP_FLAGS "$SNAP_PACKAGE"
esac
