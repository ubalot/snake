# SNAKE

## Development

### Debug
#### Development
```bash
make debug
./bin/snake
```
#### Snap
(Remember to set `confinement: devmode` in `snapcraft.yml`)
```bash
./snap.sh debug
```

---

### Release
#### Development
```bash
make
./bin/snake
```

#### Snap
(Remember to set `confinement: strict` in `snapcraft.yml`)
```bash
./snap.sh
```