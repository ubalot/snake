CXX       := g++
CXX_FLAGS := -Wall -Wextra -Wimplicit-fallthrough -std=c++11

BIN        := bin
SRC        := src
INCLUDE    := include
LIB        := lib
LIBRARIES  := -static-libstdc++ -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
EXECUTABLE := snake

SOURCES = $(notdir $(wildcard $(SRC)/*.cpp))
HEADERS = $(patsubst %,$(INCLUDE)/%,$(SOURCES:.cpp=.h))
OBJECTS = $(patsubst %,$(BIN)/%,$(SOURCES:.cpp=.o))

all: message $(BIN)/$(EXECUTABLE)
message:
	@test -d $(BIN) || mkdir $(BIN)
	@echo "🚧 Building $@"

debug: CXX_FLAGS += -DDEBUG -g -ggdb -O0 #-O1 is still acceptable for valgrind
debug: clean all

release: CXX_FLAGS += -O3 -Werror
release: clean all

# run: clean all
run: all
	clear
	@echo "🚀 Executing..."
	$(BIN)/$(EXECUTABLE)

# -o $@ -> Tells make to put the output in a file named after the target
# $^ 	-> all of the file names in $(OBJS) separated by a space
$(BIN)/$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)

# -o $@ -> Tells make to put the output in a file named after the target
# $< 	-> first element in dependencies list
$(BIN)/%.o: $(SRC)/%.cpp $(INCLUDE)/%.h
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) -c -o $@ $< $(LIBRARIES)

.PHONY: clean
clean:
	@echo "🧹 Cleaning..."
	-rm -rf $(BIN)


valgrind_memcheck: debug
	valgrind --leak-check=yes --track-origins=yes $(BIN)/$(EXECUTABLE)