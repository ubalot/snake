#include <timer.h>

using namespace timer;

time_t_ Timer::now() const
{
    return SDL_GetTicks();
}

Timer::Timer()
{
    set_speed(Speed::normal);
    m_expiration = now() + m_step;
}

Timer::~Timer()
{
}

Speed Timer::get_speed() const
{
    return m_speed;
}

void Timer::set_speed(const Speed speed)
{
    m_step = static_cast<time_t_>(speed);
    m_speed = speed;
}

void Timer::increase()
{
    m_expiration = now() + m_step;
}

bool Timer::is_valid() const
{
    return now() < m_expiration;
}

bool Timer::expired() const
{
    return !is_valid();
}
