#include <algorithm>
#include <stdlib.h>
#include <time.h>

#include <engine.h>

using namespace engine;

Grid::Grid()
{
    for (int x = gfx::GameWindow::get_instance()->width_offset();
             x < gfx::GameWindow::get_instance()->width();
             x += gfx::Tile::get_instance()->size())
        xs.push_back(x);

    for (int y = gfx::GameWindow::get_instance()->height_offset();
             y < gfx::GameWindow::get_instance()->height();
             y += gfx::Tile::get_instance()->size())
        ys.push_back(y);
}

Grid::~Grid()
{
}

bool Tile::is_out_of_bounds(const int x, const int y)
{
    return x < gfx::GameWindow::get_instance()->width_offset() ||
           y < gfx::GameWindow::get_instance()->height_offset() ||
           x >= gfx::GameWindow::get_instance()->width_offset() + gfx::GameWindow::get_instance()->width() ||
           y >= gfx::GameWindow::get_instance()->height_offset() + gfx::GameWindow::get_instance()->height();
}

Tile::Tile()
{
    m_pos = {gfx::GameWindow::get_instance()->width_offset(), gfx::GameWindow::get_instance()->height_offset()};
}

Tile::Tile(const int x, const int y)
{
    if (Tile::is_out_of_bounds(x, y))
        throw OutOfRangeError(std::string("Tile is out of window bounds."));

    m_pos = {x, y};
}

Tile::Tile(const gfx::Pos pos)
{
    if (Tile::is_out_of_bounds(pos.x, pos.y))
        throw OutOfRangeError(std::string("Tile is out of window  bounds."));

    m_pos = {pos.x, pos.y};
}

Tile::~Tile()
{
}

bool Tile::operator==(const Tile& other) const
{
    return get_pos() == other.get_pos();
}

const gfx::Pos Tile::get_pos() const
{
    return m_pos;
}

void Tile::set_pos(gfx::Pos pos)
{
    if (Tile::is_out_of_bounds(pos.x, pos.y))
        throw OutOfRangeError(std::string("Can't set tile out of window bounds."));

    m_pos = pos;
}

bool Engine::detect_collision(const Tile a, const Tile b)
{
    return a.get_pos() == b.get_pos();
}

Tile Engine::random_tile(const Tiles forbidden_positions)
{
    srand(time(NULL));

    Grid grid;

    const auto cbegin {forbidden_positions.cbegin()};
    const auto cend {forbidden_positions.cend()};

    Tile rand_pos;
    do
    {
        const int rand_x_idx = rand() % grid.xs.size();
        const int rand_y_idx = rand() % grid.ys.size();
        rand_pos = Tile(grid.xs[rand_x_idx], grid.ys[rand_y_idx]);
    }
    while (std::find(cbegin, cend, rand_pos) != cend);
    return rand_pos;
}

Engine::Engine()
    : m_direction{Direction::right}
    , m_direction_used{false}
    , m_mode{Mode::modern}
{
}

Engine::~Engine()
{
}

Direction Engine::direction() const
{
    return m_direction;
}

void Engine::set_direction(Direction direction)
{
    if (m_direction_used)
    {
        m_direction = direction;
        m_direction_used = false;
    }
}

Mode Engine::mode() const
{
    return m_mode;
}

void Engine::set_mode(Mode mode)
{
    m_mode = mode;
}

gfx::Pos Engine::move_tile(Tile tile)
{
    gfx::Pos new_pos = tile.get_pos();

    switch (m_direction)
    {
    case Direction::right:
        new_pos.x += gfx::Tile::get_instance()->size();
        if (m_mode == Mode::modern)
        {
            if (new_pos.x >= gfx::GameWindow::get_instance()->width_offset() + gfx::GameWindow::get_instance()->width())
                new_pos.x = gfx::GameWindow::get_instance()->width_offset();
        }
        break;

    case Direction::down:
        new_pos.y += gfx::Tile::get_instance()->size();
        if (m_mode == Mode::modern)
        {
            if (new_pos.y >= gfx::GameWindow::get_instance()->height_offset() + gfx::GameWindow::get_instance()->height())
                new_pos.y = gfx::GameWindow::get_instance()->height_offset();
        }
        break;

    case Direction::left:
        new_pos.x -= gfx::Tile::get_instance()->size();
        if (m_mode == Mode::modern)
        {
            if (new_pos.x < gfx::GameWindow::get_instance()->width_offset())
                new_pos.x = (gfx::GameWindow::get_instance()->width_offset() + gfx::GameWindow::get_instance()->width()) - gfx::Tile::get_instance()->size();
        }
        break;

    case Direction::up:
        new_pos.y -= gfx::Tile::get_instance()->size();
        if (m_mode == Mode::modern)
        {
            if (new_pos.y < gfx::GameWindow::get_instance()->height_offset())
                new_pos.y = (gfx::GameWindow::get_instance()->height_offset() + gfx::GameWindow::get_instance()->height()) - gfx::Tile::get_instance()->size();
        }
        break;
    }

    m_direction_used = true;

    return new_pos;
}

void Engine::handle_arrow_keys(const event::Key key)
{
    switch (key.key)
    {
    case event::KeyboardKey::UP:
        if (m_direction != Direction::down)
            m_direction = Direction::up;
        break;
    case event::KeyboardKey::RIGHT:
        if (m_direction != Direction::left)
            m_direction = Direction::right;
        break;
    case event::KeyboardKey::LEFT:
        if (m_direction != Direction::right)
            m_direction = Direction::left;
        break;
    case event::KeyboardKey::DOWN:
        if (m_direction != Direction::up)
            m_direction = Direction::down;
        break;
    default:
        break;
    }
}

