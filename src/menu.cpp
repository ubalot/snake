#ifdef DEBUG
#include <cassert>
#include <iostream>
#endif

#include <menu.h>

using namespace menu;

Container::Container(std::shared_ptr<gfx::SDL> sdl, const int x, const int y)
    : m_sdl(sdl)
    , m_container_x{x}
    , m_container_y{y}
    , m_container_w{gfx::Window::get_instance()->width() - x * 2}
    , m_container_h{gfx::Window::get_instance()->height() - y * 2}
    , m_container_border_width{8}
    , m_inner_container_x{m_container_x + m_container_border_width}
    , m_inner_container_y{m_container_y + m_container_border_width}
    , m_inner_container_w{m_container_w - m_container_border_width}
    , m_inner_container_h{m_container_h - m_container_border_width}
{
    if (x < 0 || y < 0)
        throw MenuError("[[Container]] :: x and y must be > 0");
}

Container::~Container()
{
}

void Container::draw_container()
{
    const SDL_Rect rect {m_container_x, m_container_y, m_container_w, m_container_h};
    m_sdl->draw_box(rect, m_container_border_width);
}

void Container::update_size()
{
    std::pair<int, int> position {m_sdl->get_position()};
    int x {position.first}, y {position.second};

    m_container_x = x;
    m_container_y = y;
    m_container_w = gfx::Window::get_instance()->width() - x * 2;
    m_container_h = gfx::Window::get_instance()->height() - y * 2;
    //m_container_border_width = 8;
    m_inner_container_x = m_container_x + m_container_border_width;
    m_inner_container_y = m_container_y + m_container_border_width;
    m_inner_container_w = m_container_w - m_container_border_width;
    m_inner_container_h = m_container_h - m_container_border_width;
}

bool Box::is_in_container(const int x, const int y) const
{
    const bool x_in_range {x >= m_container_x && x <= m_container_x + m_container_w + m_container_border_width};
    const bool y_in_range {y >= m_container_y && y <= m_container_y + m_container_h + m_container_border_width};

    return x_in_range && y_in_range;
}

Box::Box(std::shared_ptr<gfx::SDL> sdl, const int x, const int y, const std::string name, const BoxNotification notification, const int width, const int height)
    : Container(sdl, x, y)
    , m_name{name}
    , m_container_x{x}
    , m_container_y{y}
    , m_container_w{width}
    , m_container_h{height}
    , m_container_border_width{4}
    , m_inner_container_x{m_container_x + m_container_border_width}
    , m_inner_container_y{m_container_y + m_container_border_width}
    , m_inner_container_w{m_container_w - m_container_border_width}
    , m_inner_container_h{m_container_h - m_container_border_width}
    , m_notification{notification}
    , m_hover{false}
{
    if (x < 0 || y < 0)
        throw MenuError("[[Box]] :: x and y must be > 0");
}

Box::~Box()
{
#ifdef DEBUG
    std::cout << "Distruggo Box: " << m_name << std::endl;
#endif
}

std::string Box::name() const
{
    return m_name;
}

void Box::draw_container()
{
    m_sdl->set_draw_color(0, 0, 0, 255);

    const SDL_Rect rect {m_container_x, m_container_y, m_container_w, m_container_h};
    m_sdl->draw_box(rect, m_container_border_width);

    if (m_hover)
    {
        m_sdl->set_draw_color(255 / 2, 255 / 2, 255 / 2, 255);

        const SDL_Rect inner_rect {m_inner_container_x, m_inner_container_y, m_inner_container_w, m_inner_container_h};
        m_sdl->fill_box(inner_rect);
    }

    m_sdl->set_draw_color(0, 0, 0, 255);

    const auto w {m_container_w / 2};
    const auto h {m_container_h / 2};
    const auto x {m_container_x + w / 2};
    const auto y {m_container_y + h / 2};
    m_sdl->draw_font(m_name, {x, y, w, h});
}

bool Box::clicked(const gfx::Pos pos)
{
    return is_in_container(pos.x, pos.y);
}

void Box::hover(const gfx::Pos pos)
{
    m_hover = is_in_container(pos.x, pos.y);
}

void Box::update_size()
{
    std::pair<int, int> position {m_sdl->get_position()};
    int x {position.first}, y {position.second};

    // m_container_x = x;
    // m_container_y = y;
    // m_container_w = gfx::Window::get_instance()->width() - x * 2;
    // m_container_h = gfx::Window::get_instance()->height() - y * 2;
    // //m_container_border_width = 8;
    // m_inner_container_x = m_container_x + m_container_border_width;
    // m_inner_container_y = m_container_y + m_container_border_width;
    // m_inner_container_w = m_container_w - m_container_border_width;
    // m_inner_container_h = m_container_h - m_container_border_width;
}

MenuPage::MenuPage(std::shared_ptr<gfx::SDL> sdl, std::vector<Box> boxes)
    : Container(sdl)
    , m_boxes{boxes}
{
}

MenuPage::~MenuPage()
{
#ifdef DEBUG
    std::cout << "Distruggo MenuPage" << std::endl;
#endif
}

void MenuPage::draw_container()
{
    m_sdl->set_draw_color(0, 0, 0, 255);

    const SDL_Rect rect {m_container_x, m_container_y, m_container_w, m_container_h};
    m_sdl->draw_box(rect, m_container_border_width);
}

void MenuPage::update_size()
{
    std::pair<int, int> position {m_sdl->get_position()};
    int x {position.first}, y {position.second};

    m_container_x = x;
    m_container_y = y;
    m_container_w = gfx::Window::get_instance()->width() - x * 2;
    m_container_h = gfx::Window::get_instance()->height() - y * 2;
    //m_container_border_width = 8;
    m_inner_container_x = m_container_x + m_container_border_width;
    m_inner_container_y = m_container_y + m_container_border_width;
    m_inner_container_w = m_container_w - m_container_border_width;
    m_inner_container_h = m_container_h - m_container_border_width;

    for (Box& box : m_boxes)
    {
        box.update_size();
    }
}

Menu::Menu(std::shared_ptr<gfx::SDL> sdl)
    : Container(sdl)
    , m_active_page_idx{0}
    , m_pages{}
{
    {  // Main menu page
        std::vector<Box> main_screen_boxes {};

        const auto x {gfx::Tile::get_instance()->size() * 8};
        const auto y {gfx::Tile::get_instance()->size() * 4};
        const auto h {gfx::Tile::get_instance()->size() * 2};
        const auto extra_space_y {gfx::Tile::get_instance()->size()};
        main_screen_boxes.push_back( Box(m_sdl, x, y,                                 std::string("mode"),  BoxNotification::menu_mode_page)  );
        main_screen_boxes.push_back( Box(m_sdl, x, y + h + extra_space_y,             std::string("speed"), BoxNotification::menu_speed_page) );
        main_screen_boxes.push_back( Box(m_sdl, x, y + (h * 2) + (extra_space_y * 2), std::string("quit"),  BoxNotification::exit_game)       );

        m_pages.push_back(MenuPage(sdl, main_screen_boxes));
    }
    {  // Mode menu page
        std::vector<Box> mode_screen_boxes {};

        const auto x {gfx::Tile::get_instance()->size() * 8};
        const auto y {gfx::Tile::get_instance()->size() * 4};
        const auto h {gfx::Tile::get_instance()->size() * 2};
        const auto extra_space_y {gfx::Tile::get_instance()->size()};
        mode_screen_boxes.push_back( Box(m_sdl, x, y,                     std::string("modern"),  BoxNotification::modern_mode)  );
        mode_screen_boxes.push_back( Box(m_sdl, x, y + h + extra_space_y, std::string("classic"), BoxNotification::classic_mode) );

        m_pages.push_back(MenuPage(sdl, mode_screen_boxes));
    }
    {  // Speed menu page
        std::vector<Box> speed_screen_boxes {};

        const auto extra_space_x {gfx::Tile::get_instance()->size() * 3};
        const auto x_left {gfx::Tile::get_instance()->size() * 6};
        const auto x_right {gfx::Tile::get_instance()->size() * 14 + extra_space_x};
        const auto extra_space_y {gfx::Tile::get_instance()->size() / 2};
        const auto y {gfx::Tile::get_instance()->size() * 4};
        const auto offset_y {gfx::Tile::get_instance()->size() * 3};
        const auto w {gfx::Tile::get_instance()->size() * 10};
        const auto h {gfx::Tile::get_instance()->size() * 2};
        speed_screen_boxes.push_back( Box(m_sdl, x_left, y,                                   std::string("mega slow"),      BoxNotification::mega_slow_speed,      w, h) );
        speed_screen_boxes.push_back( Box(m_sdl, x_left, y + offset_y + extra_space_y,        std::string("super slow"),     BoxNotification::super_slow_speed,     w, h) );
        speed_screen_boxes.push_back( Box(m_sdl, x_left, y + (offset_y + extra_space_y) * 2,  std::string("very very slow"), BoxNotification::very_very_slow_speed, w, h) );
        speed_screen_boxes.push_back( Box(m_sdl, x_left, y + (offset_y + extra_space_y) * 3,  std::string("very slow"),      BoxNotification::very_slow_speed,      w, h) );
        speed_screen_boxes.push_back( Box(m_sdl, x_left, y + (offset_y + extra_space_y) * 4,  std::string("slow"),           BoxNotification::slow_speed,           w, h) );
        speed_screen_boxes.push_back( Box(m_sdl, x_right, y,                                  std::string("normal"),         BoxNotification::normal_speed,         w, h) );
        speed_screen_boxes.push_back( Box(m_sdl, x_right, y + offset_y + extra_space_y,       std::string("fast"),           BoxNotification::fast_speed,           w, h) );
        speed_screen_boxes.push_back( Box(m_sdl, x_right, y + (offset_y + extra_space_y) * 2, std::string("very fast"),      BoxNotification::very_fast_speed,      w, h) );
        speed_screen_boxes.push_back( Box(m_sdl, x_right, y + (offset_y + extra_space_y) * 3, std::string("very very fast"), BoxNotification::very_very_fast_speed, w, h) );
        speed_screen_boxes.push_back( Box(m_sdl, x_right, y + (offset_y + extra_space_y) * 4, std::string("super fast"),     BoxNotification::super_fast_speed,     w, h) );

        m_pages.push_back(MenuPage(sdl, speed_screen_boxes));
    }
}

Menu::~Menu()
{
#ifdef DEBUG
    std::cout << "Distruggo Menu" << std::endl;
#endif
}
