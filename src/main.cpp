#include <iostream>
#include <cstdlib>
#include <string>
#include <thread>

#include <game.h>
#include <gfx.h>
#include <main.h>
#include <signal_handler.h>

#ifdef __unix__
int main()
#elif defined(_WIN32)
int main(int argc, char* argv[])
#endif
{
    try
    {
        signal_handler::SignalHandler signalHandler;

        game::Game game;
        game.run();
    }
    catch (const signal_handler::SignalException& err)
	{
        std::cerr << "[Signal] :: " << err.what() << std::endl;
        return EXIT_FAILURE;
	}
    catch (const gfx::SDLError& err)
    {
        std::cerr << "[SDL] :: " << err.what() << std::endl;
        return -1;
    }
    catch (const engine::OutOfRangeError& err)
    {
        std::cerr << "[Engine] :: " << err.what() << std::endl;
        return -2;
    }
    return EXIT_SUCCESS;
}
