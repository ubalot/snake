#include <sstream>
#include <string>

#ifdef DEBUG
#include <cassert>
#include <iostream>
#endif

#include <game.h>

using namespace game;

Prey::Prey(const gfx::Pos pos)
{
    m_tile.set_pos(pos);
}

Prey::~Prey()
{
}

engine::Tile Prey::tile() const
{
    return m_tile;
}

void Prey::set_pos(const gfx::Pos pos)
{
    m_tile = pos;
}

Snake::Snake(const unsigned int length)
{
    if (length < 1)
        throw engine::OutOfRangeError(std::string("Can't create snake with length < 1"));
    else if (length >= 10)
        throw engine::OutOfRangeError(std::string("Can't create snake with length >= 10"));

    for (int i = length - 1; i >=0; i--)
        m_tiles.push_back(engine::Tile(i * gfx::Tile::get_instance()->size(), gfx::GameWindow::get_instance()->height_offset()));

    m_original_size = length;
}

Snake::~Snake()
{
}

engine::Tile Snake::head() const
{
    return m_tiles.at(0);
}

engine::Tiles Snake::tail() const
{
#ifdef DEBUG
    assert(!m_tiles.empty());
#endif

    engine::Tiles tail_ {};
    for (auto it = m_tiles.begin() + 1; it != m_tiles.end(); ++it)
        tail_.push_back(*it);
    return tail_;
}

void Snake::increase()
{
#ifdef DEBUG
    assert(!m_tiles.empty());
#endif

    engine::Tile last_tile = m_tiles[m_tiles.size() - 1];
    m_tiles.push_back(engine::Tile(last_tile.get_pos()));
}

void Snake::move_to(const gfx::Pos new_pos)
{
    for (auto i = m_tiles.size() - 1; i > 0; i--)
        m_tiles[i].set_pos(m_tiles[i - 1].get_pos());
    m_tiles[0].set_pos(new_pos);
}

engine::Tiles Snake::tiles() const
{
    return m_tiles;
}

unsigned int Snake::original_size() const
{
    return m_original_size;
}

void Game::draw_background()
{
    m_sdl->set_draw_color(255 - 10, 255 - 10, 255 - 25, 0);
    m_sdl->draw_background();

    m_sdl->set_draw_color(0, 0, 0, 255);
    m_sdl->draw_header();
}

void Game::draw_prey()
{
    if (m_pause)
        m_sdl->set_draw_color(0, 255 / 2, 0, 255 / 2);
    else
        m_sdl->set_draw_color(0, 255, 0, 255);
    m_sdl->draw_tile(m_prey.tile().get_pos().x, m_prey.tile().get_pos().y, gfx::Tile::get_instance()->size(), gfx::Tile::get_instance()->size());
}

void Game::draw_snake()
{
    if (m_pause)
        m_sdl->set_draw_color(255 / 2, 255 / 2, 255 / 2, 255 / 2);
    else
        m_sdl->set_draw_color(0, 0, 0, 255);

    for (const engine::Tile& tile : m_snake.tiles())
        m_sdl->draw_tile(tile.get_pos().x, tile.get_pos().y, gfx::Tile::get_instance()->size(), gfx::Tile::get_instance()->size());
}

void Game::draw_speed()
{
    std::ostringstream text;
    text << "Speed: " << m_points_table.at(m_timer.get_speed()) << "/10";
    const int x {gfx::GameWindow::get_instance()->width() - gfx::Tile::get_instance()->size() * 5};
    const int y {0};
    const int w {gfx::Tile::get_instance()->size() * 5};
    const int h {gfx::Tile::get_instance()->size()};
    m_sdl->draw_font(text.str(), {x, y, w, h});
}

void Game::draw_mode()
{
    std::string text {"Mode: "};
    switch (m_engine.mode())
    {
    case engine::Mode::classic: text += "classic"; break;
    case engine::Mode::modern:  text += "modern";  break;
    }
    const int x {(gfx::GameWindow::get_instance()->width() / 2) - (gfx::Tile::get_instance()->size() * 4)};
    const int y {0};
    const int w {gfx::Tile::get_instance()->size() * 8};
    const int h {gfx::Tile::get_instance()->size()};
    m_sdl->draw_font(text, {x, y, w, h});
}

void Game::draw_score()
{
    std::ostringstream text;
    text << "Score: " << get_score();
    const int x {0};
    const int y {0};
    const int w {gfx::Tile::get_instance()->size() * 5};
    const int h {gfx::Tile::get_instance()->size()};
    m_sdl->draw_font(text.str(), {x, y, w, h});
}

void Game::draw_opaque_layer()
{
    m_sdl->draw_opaque_layer(gfx::GameWindow::get_instance()->width(), gfx::GameWindow::get_instance()->height());
}

bool Game::snake_eats_itself()
{
    const engine::Tile head {m_snake.head()};
    for (const engine::Tile& tile : m_snake.tail())
        if (m_engine.detect_collision(head, tile))
            return true;
    return false;
}

bool Game::snake_eats_prey()
{
    return m_engine.detect_collision(m_snake.head(), m_prey.tile());
}

void Game::prey_new_pos()
{
    gfx::Pos new_pos {m_engine.random_tile(m_snake.tiles()).get_pos()};
    m_prey.set_pos(new_pos);
}

unsigned long long Game::get_score() const
{
    unsigned long long eaten_preys {m_snake.tiles().size() - m_snake.original_size()};
    return eaten_preys * m_points_table.at(m_timer.get_speed());
}

Game::Game()
    : m_quit{false}
    , m_pause{true}
    , m_game_over{false}
    , m_sdl(new gfx::SDL(SDL_INIT_VIDEO | SDL_INIT_TIMER))
    , m_events_manager(event::EventsManager())
    , m_snake(Snake())
    , m_prey(Prey())
    , m_timer(timer::Timer())
    , m_menu(menu::Menu(m_sdl))
    , m_engine(engine::Engine())
{
    prey_new_pos();
}

Game::~Game()
{
}

void Game::handle_input()
{
    if (m_events_manager.capture())
    {
        const event::EventHandler event_handler {m_events_manager.pop()};
        const event::Event event {event_handler.event};
        const event::Key key {event_handler.key};
        int x {event_handler.x};
        int y {event_handler.y};

#ifdef DEBUG
        std::cout << "Event: " << event << " "
                  << "key: " << key << " "
                  << "x: " << x << " "
                  << "y: " << y << std::endl;
#endif

        switch (event.type)
        {
        case event::EventType::SystemEvent:
            switch (event.system_event)
            {
            case event::SystemEvent::window_quit:
                m_quit = true;
                break;

            case event::SystemEvent::window_hidden:
            case event::SystemEvent::window_moved:
            case event::SystemEvent::window_minimized:
            case event::SystemEvent::window_focus_lost:
                m_pause = true;
                break;

            case event::SystemEvent::window_maximized:
                x = m_sdl->get_window_size().first;
                y = m_sdl->get_window_size().second;
                gfx::Window::get_instance()->resize(x, y);
                gfx::GameWindow::get_instance()->update_size();
                gfx::Tile::get_instance()->update_size();
                m_menu.update_size();
                restart();
                m_pause = true;
                break;
            case event::SystemEvent::window_size_changed:
                break;

            case event::SystemEvent::window_resized:
                gfx::Window::get_instance()->resize(x, y);
                gfx::GameWindow::get_instance()->update_size();
                gfx::Tile::get_instance()->update_size();
                m_sdl->set_window_size(gfx::Window::get_instance()->width(), gfx::Window::get_instance()->height());
                m_menu.update_size();
                restart();
                m_pause = true;
                break;

            // for more info: https://wiki.libsdl.org/SDL_WindowEvent , check it out!
            default:
                break;
            }
            break;

        case event::EventType::KeyboardEvent:
            switch (event.keyboard_event)
            {
            case event::KeyboardEvent::key_pressed:
                switch (key.key)
                {
                case event::KeyboardKey::ESC:
                    m_pause = !m_pause;
                    break;
                default:
                    m_engine.handle_arrow_keys(key);
                    break;
                }
                break;
            default:
                break;
            }
            break;

        case event::EventType::MouseEvent:
            if (m_pause)
            {
                gfx::Pos pos {x, y};
                menu::MenuPage& mp = m_menu.get_active_page();
                switch (event.mouse_event)
                {
                case event::MouseEvent::mouse_clicked:
                    {
                        int idx = 0;
                        for (menu::Box& box : mp.boxes())
                        {
                            if (box.clicked(pos))
                            {
                                handle_menu_notification(box.notification(), idx);
                            }
                            idx++;
                        }
                        break;
                    }
                case event::MouseEvent::mouse_released:
                    break;
                case event::MouseEvent::mouse_motion:
                    for (menu::Box& box : mp.boxes())
                    {
                        box.hover(pos);
                    }
                    break;
                default:
                    break;
                }
            }
            break;

        default:
            break;
        }
    }
}

void Game::check_state()
{
    if (!m_game_over)
    {
        if (m_timer.expired() && !m_pause)
        {
            m_snake.move_to(m_engine.move_tile(m_snake.head()));

            if (snake_eats_itself())
            {
                m_game_over = true;
                m_pause = true;
            }

            if (snake_eats_prey())
            {
                m_snake.increase();
                m_prey.set_pos(m_engine.random_tile(m_snake.tiles()).get_pos());
            }

            m_timer.increase();
        }
    }
    else
    {
        // game over: show scores = `eaten_preys`* level_diff

        m_pause = true;
    }

    if (m_pause)
    {
        //draw menu

        m_timer.increase();
    }
}

void Game::update_screen()
{
    draw_background();

    draw_speed();
    draw_mode();
    draw_score();

    draw_prey();
    draw_snake();

    if (m_pause)
    {
        draw_opaque_layer();

        menu::MenuPage& mp = m_menu.get_active_page();
        for (menu::Box& box : mp.boxes())
        {
            box.draw_container();
        }
        m_menu.update_screen();
    }
    else
    {
        // Restore main page for menu (for next time it will be invoked)
        m_menu.set_active_page(0);
    }
}

void Game::render()
{
    m_sdl->render();
}

void Game::handle_sounds()
{
}

void Game::handle_menu_notification(const menu::BoxNotification notification, const int menu_active_page_idx)
{
    switch (notification)
    {
        // main menu page
        case menu::BoxNotification::menu_mode_page:       m_menu.set_active_page(menu_active_page_idx + 1); break;
        case menu::BoxNotification::menu_speed_page:      m_menu.set_active_page(menu_active_page_idx + 1); break;
        case menu::BoxNotification::exit_game:            exit();                                           break;

        // mode menu page
        case menu::BoxNotification::classic_mode:         m_engine.set_mode(engine::Mode::classic); restart(); break;
        case menu::BoxNotification::modern_mode:          m_engine.set_mode(engine::Mode::modern);  restart(); break;

        // speed menu page
        case menu::BoxNotification::mega_slow_speed:      m_timer.set_speed(timer::Speed::mega_slow);      restart(); break;
        case menu::BoxNotification::super_slow_speed:     m_timer.set_speed(timer::Speed::super_slow);     restart(); break;
        case menu::BoxNotification::very_very_slow_speed: m_timer.set_speed(timer::Speed::very_very_slow); restart(); break;
        case menu::BoxNotification::very_slow_speed:      m_timer.set_speed(timer::Speed::very_slow);      restart(); break;
        case menu::BoxNotification::slow_speed:           m_timer.set_speed(timer::Speed::slow);           restart(); break;
        case menu::BoxNotification::normal_speed:         m_timer.set_speed(timer::Speed::normal);         restart(); break;
        case menu::BoxNotification::fast_speed:           m_timer.set_speed(timer::Speed::fast);           restart(); break;
        case menu::BoxNotification::very_fast_speed:      m_timer.set_speed(timer::Speed::very_fast);      restart(); break;
        case menu::BoxNotification::very_very_fast_speed: m_timer.set_speed(timer::Speed::very_very_fast); restart(); break;
        case menu::BoxNotification::super_fast_speed:     m_timer.set_speed(timer::Speed::super_fast);     restart(); break;
    }
}

void Game::restart()
{
    m_game_over = false;
    m_pause = false;
    m_snake = Snake();
    m_prey = Prey();
    prey_new_pos();
    m_engine.set_direction(engine::Direction::right);
}

void Game::run()
{
    while (!m_quit)
    {
        std::cout << "width: " << gfx::GameWindow::get_instance()->width()
                  << " height: " << gfx::GameWindow::get_instance()->height()
                  << " tile size: " << gfx::Tile::get_instance()->size() <<std::endl;
        handle_input();
        check_state();
        update_screen();
        render();
        handle_sounds();
    }
}
