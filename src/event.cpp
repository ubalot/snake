#include <event.h>

using namespace event;

EventsManager::EventsManager()
{
}

EventsManager::~EventsManager()
{
}

Key handle_keyboard_event(const SDL_Event event)
{
    Key k {KeyboardKey::RETURN};
    switch (event.key.keysym.sym)
    {
    // keyboard
    case SDLK_RETURN:     k.key = KeyboardKey::RETURN; break;
    case SDLK_ESCAPE:     k.key = KeyboardKey::ESC;    break;
    case SDLK_UP:         k.key = KeyboardKey::UP;     break;
    case SDLK_RIGHT:      k.key = KeyboardKey::RIGHT;  break;
    case SDLK_LEFT:       k.key = KeyboardKey::LEFT;   break;
    case SDLK_DOWN:       k.key = KeyboardKey::DOWN;   break;
    }
    return k;
}

Key handle_mouse_event(const SDL_Event event)
{
    Key k {KeyboardKey::RETURN};
    switch (event.button.button)
    {
    // mouse
    case SDL_BUTTON_LEFT: k.button = MouseButton::LEFT_BUTTON; break;
    default:              k.button = MouseButton::NO_EVENT;    break;
    }
    return k;
}

Key handle_motion_event(const SDL_Event event, int &x, int &y)
{
    Key k;
    x = event.motion.x;
    y = event.motion.y;
    switch (event.motion.type)
    {
    case SDL_MOUSEMOTION: k.button = MouseButton::NO_EVENT; break;
    default:              k.button = MouseButton::NO_EVENT; break;
    }
    return k;
}

Key handle_window_size(const SDL_Event event, int &x, int &y)
{
    x = event.window.data1;
    y = event.window.data2;
    return {KeyboardKey::RETURN};  // just for code consistency
}

bool EventsManager::capture()
{
    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        Event e;
        e.type = EventType::NoEvent;
        Key k;
        int x {0};
        int y {0};

        switch (event.type)
        {
        // keyboard
        case SDL_KEYDOWN:
            e.type = EventType::KeyboardEvent;
            e.keyboard_event = KeyboardEvent::key_pressed;
            k = handle_keyboard_event(event);
            break;
        case SDL_KEYUP:
            e.type = EventType::KeyboardEvent;
            e.keyboard_event = KeyboardEvent::key_released;
            k = handle_keyboard_event(event);
            break;

        // mouse
        case SDL_MOUSEBUTTONDOWN:
            e.type = EventType::MouseEvent;
            e.mouse_event = MouseEvent::mouse_clicked;
            k = handle_mouse_event(event);
            SDL_GetMouseState(&x, &y);
            break;
        case SDL_MOUSEMOTION:
            e.type = EventType::MouseEvent;
            e.mouse_event = MouseEvent::mouse_motion;
            k = handle_motion_event(event, x, y);
            break;

        //system
        case SDL_QUIT:
            e.type = EventType::SystemEvent;
            e.system_event = SystemEvent::window_quit;
            break;

        case SDL_WINDOWEVENT:
            switch (event.window.event)
            {
            case SDL_WINDOWEVENT_HIDDEN:
                e.type = EventType::SystemEvent;
                e.system_event = SystemEvent::window_hidden;
                break;

            case SDL_WINDOWEVENT_MOVED:
                e.type = EventType::SystemEvent;
                e.system_event = SystemEvent::window_moved;
                break;

            case SDL_WINDOWEVENT_MINIMIZED:
                e.type = EventType::SystemEvent;
                e.system_event = SystemEvent::window_minimized;
                break;

            case SDL_WINDOWEVENT_MAXIMIZED:
                e.type = EventType::SystemEvent;
                e.system_event = SystemEvent::window_maximized;
                k = handle_window_size(event, x, y);
                break;

            case  SDL_WINDOWEVENT_FOCUS_LOST:
                e.type = EventType::SystemEvent;
                e.system_event = SystemEvent::window_focus_lost;
                break;

            case SDL_WINDOWEVENT_RESIZED:
                e.type = EventType::SystemEvent;
                e.system_event = SystemEvent::window_resized;
                k = handle_window_size(event, x, y);
                break;

            case SDL_WINDOWEVENT_SIZE_CHANGED:
                e.type = EventType::SystemEvent;
                e.system_event = SystemEvent::window_size_changed;
                k = handle_window_size(event, x, y);
                break;

            default:
                break;
            }
            break;
        }

        m_queue.push({e, k, x, y});

        return true;
    }

    return false;
}

EventHandler EventsManager::pop()
{
    EventHandler event_handler = m_queue.front();
    m_queue.pop();
    return event_handler;
}
