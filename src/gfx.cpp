#include <gfx.h>

using namespace gfx;

const std::vector<std::pair<int, int>> Window::resolutions
{
    {256,	144},
    {426, 	240},
    {640, 	360},   // nHD
    {768, 	432},
    {800, 	450},
    {848, 	480},
    {854, 	480},   // FWVGA
    {960, 	540},   // qHD
    {1024, 	576},
    {1280, 	720},   // HD
    {1366, 	768},   // WXGA
    {1600, 	900},   // HD+
    {1920, 	1080},  // Full HD
    {2048, 	1152},
    {2560, 	1440},  // QHD
    {2880, 	1620},
    {3200, 	1800},  // QHD+
    {3840, 	2160},  // 4K UHD
    {4096, 	2304},
    {5120, 	2880},  // 5K
    {7680, 	4320},  // 8K UHD
    {15360, 8640}   // 16K UHD
};

SDL::SDL(Uint32 flags)
{
    if (SDL_Init(flags) != 0)
        throw SDLError();

    if (TTF_Init() == -1)
		throw SDLError(TTF_GetError());

    m_window = SDL_CreateWindow(
        "Snake",                                 // window title
        SDL_WINDOWPOS_UNDEFINED,                 // initial x position
        SDL_WINDOWPOS_UNDEFINED,                 // initial y position
        Window::get_instance()->width(),         // width, in pixels
        Window::get_instance()->height(),        // height, in pixels
        SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE  // flags
    );
    if (!m_window)
        throw SDLError();

    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!m_renderer)
        throw SDLError();

    if (SDL_SetRenderDrawBlendMode(m_renderer, SDL_BLENDMODE_BLEND) != 0)
        throw SDLError();

    m_window_size = get_window_size();

    m_font = TTF_OpenFont(Menu_Font, 28);
    if (!m_font)
        throw SDLError(TTF_GetError());
}

SDL::~SDL()
{
    TTF_CloseFont(m_font);
    SDL_DestroyWindow(m_window);
    SDL_DestroyRenderer(m_renderer);
    TTF_Quit();
	// IMG_Quit();
    SDL_Quit();
}

int SDL::window_width()
{
    return m_window_size.first;
}

int SDL::window_height()
{
    return m_window_size.second;
}

void SDL::set_draw_color(const Uint8 r, const Uint8 g, const Uint8 b, const Uint8 a)
{
    if (SDL_SetRenderDrawColor(m_renderer, r, g, b, a) != 0)
        throw SDLError();
}

void SDL::draw_background()
{
    // apply setted color to all screen
    if (SDL_RenderClear(m_renderer) != 0)
        throw SDLError();
}

void SDL::draw_header()
{  // draw separation from score and actuale game window
    const int thickness {2};
    const SDL_Rect rect {0, GameWindow::get_instance()->height_offset() - thickness, GameWindow::get_instance()->width(), thickness};
    SDL_RenderFillRect(m_renderer, &rect);
}

void SDL::draw_tile(int x, int y, int w, int h)
{
    const SDL_Rect rect {x, y, w, h};
    SDL_RenderFillRect(m_renderer, &rect);
}

void SDL::render()
{
    SDL_RenderPresent(m_renderer);  // Show the window
}

void SDL::draw_box(const SDL_Rect rect, const int line_width)
{
    const SDL_Rect rect_top_border = {rect.x, rect.y, rect.w, line_width};
    SDL_RenderFillRect(m_renderer, &rect_top_border);
    const SDL_Rect rect_left_border = {rect.x, rect.y, line_width, rect.h};
    SDL_RenderFillRect(m_renderer, &rect_left_border);
    const SDL_Rect rect_bottom_border = {rect.x, rect.y + rect.h, rect.w, line_width};
    SDL_RenderFillRect(m_renderer, &rect_bottom_border);
    const SDL_Rect rect_right_border = {rect.x + rect.w, rect.y, line_width, rect.h + line_width};
    SDL_RenderFillRect(m_renderer, &rect_right_border);
}

void SDL::draw_font(const std::string text, const SDL_Rect rect)
{
    SDL_Color color {0, 0, 0, 0};
    SDL_Surface* surface = TTF_RenderText_Solid(m_font, text.c_str(), color);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(m_renderer, surface);

    SDL_RenderCopy(m_renderer, texture, NULL, &rect);

    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surface);
}

void SDL::fill_box(const SDL_Rect rect)
{
    SDL_RenderFillRect(m_renderer, &rect);
}

void SDL::draw_opaque_layer(const int w, const int h)
{
    SDL_SetRenderDrawColor(m_renderer, 255 / 2, 255 / 2, 255 / 2, 255 / 3);
    const auto x {gfx::GameWindow::get_instance()->width_offset()};
    const auto y {gfx::GameWindow::get_instance()->height_offset()};
    SDL_Rect rect {x, y, w, h};
    SDL_RenderFillRect(m_renderer, &rect);
}

std::pair<int, int> SDL::get_window_size()
{
    int width {0}, height {0};
    if (SDL_GetRendererOutputSize(m_renderer, &width, &height) != 0)
        throw SDLError();
    m_window_size = {width, height};
    return m_window_size;
}

std::pair<int, int> SDL::get_position() {
    int x, y;
    SDL_GetWindowPosition(m_window, &x, &y);
    return std::pair<int, int>{x, y};
}

void SDL::set_window_size(const int w, const int h) {
    SDL_SetWindowSize(m_window, w, h);
}
