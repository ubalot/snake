#include <signal.h>
#include <errno.h>

#include <signal_handler.h>

using namespace signal_handler;

SignalHandler::SignalHandler()
{
    setup_signal_handlers();
}

SignalHandler::~SignalHandler()
{
}

void SignalHandler::exit_signal_handler(int signum)
{
    throw SignalException(std::to_string(signum));
}

void SignalHandler::setup_signal_handlers()
{
    // Set up the signal handlers for CTRL-C.
    if (signal((int) SIGINT, SignalHandler::exit_signal_handler) == SIG_ERR ||
        signal((int) SIGABRT, SignalHandler::exit_signal_handler) == SIG_ERR)
    {
        throw SignalException("!!!!! Error setting up signal handlers !!!!!");
    }
}
