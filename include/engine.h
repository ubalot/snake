#ifndef ENGINE_H
#define ENGINE_H

#include <exception>
#include <string>
#include <vector>

#include <event.h>
#include <gfx.h>

namespace engine
{
    enum class Direction
    {
        right,
        down,
        left,
        up
    };

    enum class Mode
    {
        classic,
        modern
    };

    struct Grid
    {
        std::vector<int> xs;
        std::vector<int> ys;
        Grid();
        ~Grid();
    };

    class Tile
    {
        gfx::Pos m_pos;
        static bool is_out_of_bounds(const int x, const int y);

    public:
        Tile();
        Tile(const int x, const int y);
        Tile(const gfx::Pos pos);
        ~Tile();
        bool operator==(const Tile& other) const;
        const gfx::Pos get_pos() const;
        void set_pos(gfx::Pos pos);
    };

    using Tiles = std::vector<Tile>;

    class OutOfRangeError : public std::exception
    {
        std::string msg;

    public:
        OutOfRangeError(const std::string& msg) : exception(), msg(msg) {}
        virtual ~OutOfRangeError() throw() {}
        virtual const char * what() const throw() { return msg.c_str(); }
    };

    class Engine
    {
        Direction m_direction;
        bool m_direction_used;

        Mode m_mode;

    public:
        Engine();
        ~Engine();

        Direction direction() const;
        void set_direction(Direction);

        Mode mode() const;
        void set_mode(Mode);

        gfx::Pos move_tile(Tile);
        bool detect_collision(const Tile, const Tile);
        Tile random_tile(const Tiles);

        void handle_arrow_keys(const event::Key);
    };
}

#endif
