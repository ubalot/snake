#ifndef GAME_H
#define GAME_H

#include <functional>
#include <map>
#include <vector>

#include <engine.h>
#include <menu.h>
#include <timer.h>

namespace game
{
    class Prey
    {
        engine::Tile m_tile;

    public:
        Prey(const gfx::Pos pos = {gfx::GameWindow::get_instance()->width_offset(), gfx::GameWindow::get_instance()->height_offset()});
        ~Prey();
        engine::Tile tile() const;
        void set_pos(const gfx::Pos pos);
    };

    class Snake
    {
        unsigned int m_original_size;
        engine::Tiles m_tiles;

    public:
        Snake(const unsigned int length = 3);
        ~Snake();
        // const engine::Direction direction() const;
        engine::Tile head() const;
        engine::Tiles tail() const;
        void increase();
        void move_to(const gfx::Pos new_pos);
        engine::Tiles tiles() const;
        unsigned int original_size() const;
    };

    class Game
    {
        const std::map<timer::Speed, unsigned long long> m_points_table {
            {timer::Speed::mega_slow      ,  1},
            {timer::Speed::super_slow     ,  2},
            {timer::Speed::very_very_slow ,  3},
            {timer::Speed::very_slow      ,  4},
            {timer::Speed::slow           ,  5},
            {timer::Speed::normal         ,  6},
            {timer::Speed::fast           ,  7},
            {timer::Speed::very_fast      ,  8},
            {timer::Speed::very_very_fast ,  9},
            {timer::Speed::super_fast     , 10}
        };

        bool m_quit;
        bool m_pause;
        bool m_game_over;
        std::shared_ptr<gfx::SDL> m_sdl;
        event::EventsManager m_events_manager;
        Snake m_snake;
        Prey m_prey;
        timer::Timer m_timer;
        menu::Menu m_menu;
        engine::Engine m_engine;

        void draw_background();
        void draw_prey();
        void draw_snake();
        void draw_speed();
        void draw_mode();
        void draw_score();
        void draw_opaque_layer();

        bool snake_eats_itself();
        bool snake_eats_prey();
        void prey_new_pos();

        unsigned long long get_score() const;

    protected:
        void handle_input();
        void check_state();
        void update_screen();
        void render();
        void handle_sounds();

        void handle_menu_notification(const menu::BoxNotification notification, const int idx);
        void restart();

    public:
        Game();
        ~Game();
        void run();
        void exit() { m_quit = true; };
    };
}

#endif
