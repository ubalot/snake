#ifndef TIMER_H
#define TIMER_H

#include <gfx.h>

namespace timer
{
    using time_t_ = Uint32;

    static constexpr time_t_ normal {250};
    enum class Speed
    {
        mega_slow      = normal + 250,
        super_slow     = normal + 200,
        very_very_slow = normal + 150,
        very_slow      = normal + 100,
        slow           = normal + 50,
        normal         = normal,
        fast           = normal - 50,
        very_fast      = normal - 100,
        very_very_fast = normal - 150,
        super_fast     = normal - 200
    };

    class Timer
    {
        time_t_ m_expiration;
        time_t_ m_step;
        Speed m_speed;
        time_t_ now() const;

    public:
        Timer();
        ~Timer();
        Speed get_speed() const;
        void set_speed(const Speed speed);
        void increase();
        bool is_valid() const;
        bool expired() const;
    };
}

#endif
