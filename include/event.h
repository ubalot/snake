#ifndef EVENT_H
#define EVENT_H

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

#ifdef __unix__
#include <SDL2/SDL.h>
#elif defined(_WIN32)
#include <SDL.h>
#endif

namespace event
{
    enum class SystemEvent
    {
        window_quit,
        window_hidden,
        window_moved,
        window_minimized,
        window_maximized,
        window_focus_lost,
        window_resized,
        window_size_changed
    };

    enum class KeyboardEvent
    {
        key_pressed,
        key_released,
    };

    enum class MouseEvent
    {
        mouse_clicked,
        mouse_released,
        mouse_motion
    };

    enum class EventType
    {
        NoEvent,
        SystemEvent,
        KeyboardEvent,
        MouseEvent
    };

    struct Event
    {
        EventType type;
        union
        {
            SystemEvent system_event;
            KeyboardEvent keyboard_event;
            MouseEvent mouse_event;
        };
    };

#ifdef DEBUG
    std::ostream& operator<<(std::ostream& os, const Event& e) {
        std::string value;
        switch (e.type) {
        case EventType::NoEvent:
            value = "NoEvent";
            break;
        case EventType::SystemEvent:
            value = "SystemEvent";
            value += " ";
            switch (e.system_event) {
            case SystemEvent::window_quit: value += "window_quit"; break;
            case SystemEvent::window_hidden: value += "window_hidden"; break;
            case SystemEvent::window_moved: value += "window_moved"; break;
            case SystemEvent::window_minimized: value += "window_minimized"; break;
            case SystemEvent::window_maximized: value += "window_maximized"; break;
            case SystemEvent::window_focus_lost: value += "window_focus_lost"; break;
            case SystemEvent::window_resized: value += "window_resized"; break;
            case SystemEvent::window_size_changed: value += "window_size_changed"; break;
            }
            break;
        case EventType::KeyboardEvent:
            value = "KeyboardEvent";
            break;
        case EventType::MouseEvent:
            value = "MouseEvent";
            break;
        }
        return os << value;
    }
#endif

    enum class KeyboardKey
    {
        RETURN,
        ESC,
        RIGHT,
        LEFT,
        UP,
        DOWN
    };

    enum class MouseButton
    {
        NO_EVENT,
        LEFT_BUTTON
    };

    union Key
    {
        KeyboardKey key;
        MouseButton button;
    };

    struct EventHandler
    {
        Event event;
        Key key;
        int x;
        int y;
    };

    class EventsManager
    {
        std::queue<EventHandler> m_queue;

    public:
        EventsManager();
        ~EventsManager();
        bool capture();
        EventHandler pop();
    };
}

#endif
