#ifndef SIGNALHANDLER_H
#define SIGNALHANDLER_H

#include <stdexcept>

namespace signal_handler
{
    class SignalException : public std::runtime_error
    {
    public:
        SignalException(const std::string& m) : std::runtime_error(m) {}
        virtual const char* what() const throw() { return std::runtime_error::what(); }
    };

    class SignalHandler
    {
        void setup_signal_handlers();

    public:
        SignalHandler();
        ~SignalHandler();
        static void exit_signal_handler(int);
    };
}

#endif
