#ifndef MAIN_H
#define MAIN_H

#ifdef __unix__
int main();
#elif defined(_WIN32)
int main(int argc, char* argv[]);
#endif

#endif
