#ifndef MENU_H
#define MENU_H

#include <algorithm>
#include <memory>
#include <vector>

#include <gfx.h>

namespace menu
{
    class Container
    {
    protected:
        std::shared_ptr<gfx::SDL> m_sdl;

        int m_container_x;
        int m_container_y;
        int m_container_w;
        int m_container_h;
        int m_container_border_width;

        int m_inner_container_x;
        int m_inner_container_y;
        int m_inner_container_w;
        int m_inner_container_h;

    public:
        Container(std::shared_ptr<gfx::SDL> sdl, const int x = gfx::Tile::get_instance()->size() * 4, const int y = gfx::Tile::get_instance()->size() * 2);
        ~Container();
        virtual void draw_container();
        std::pair<int, int> container_size() const { return std::pair<int, int>{m_container_w, m_container_h}; }
        virtual void update_size();
    };

    enum class BoxNotification {
        // main menu page buttons
        menu_mode_page,
        menu_speed_page,
        exit_game,

        // mode menu page
        classic_mode,
        modern_mode,

        // speed menu page
        mega_slow_speed,
        super_slow_speed,
        very_very_slow_speed,
        very_slow_speed,
        slow_speed,
        normal_speed,
        fast_speed,
        very_fast_speed,
        very_very_fast_speed,
        super_fast_speed
    };

    class Box : public Container
    {
        std::string m_name;

        int m_container_x;
        int m_container_y;
        int m_container_w;
        int m_container_h;
        int m_container_border_width;

        int m_inner_container_x;
        int m_inner_container_y;
        int m_inner_container_w;
        int m_inner_container_h;

        BoxNotification m_notification;

        bool m_hover;

    protected:
        bool is_in_container(const int x, const int y) const;

    public:
        Box(std::shared_ptr<gfx::SDL> sdl, const int x, const int y, const std::string name, const BoxNotification notification, const int width = gfx::Tile::get_instance()->size() * 16, const int height = gfx::Tile::get_instance()->size() * 2);
        ~Box();
        std::string name() const;
        void draw_container() override;
        bool clicked(const gfx::Pos pos);
        BoxNotification notification() const { return m_notification; }
        void hover(const gfx::Pos pos);
        void update_size() override;
    };

    class MenuPage : public Container
    {
        std::vector<Box> m_boxes;

    public:
        MenuPage(std::shared_ptr<gfx::SDL> sdl, std::vector<Box> boxes);
        ~MenuPage();
        std::vector<Box>& boxes() { return m_boxes; };
        void draw_container() override;
        void update_size() override;
    };

    class Menu : public Container
    {
        int m_active_page_idx;
        std::vector<MenuPage> m_pages;

    public:
        Menu(std::shared_ptr<gfx::SDL>);
        ~Menu();
        void update_screen() {
            m_pages[m_active_page_idx].draw_container();
        };
        MenuPage& get_active_page() { return m_pages[m_active_page_idx]; };
        void set_active_page(const int index) {
            m_active_page_idx = static_cast<size_t>(index) < m_pages.size() ? index : 0;
        };
        int get_active_page_idx() const { return m_active_page_idx; };
        // void update_size() override {
        //     for (MenuPage& mp : m_pages)
        //     {
        //         mp.update_size();
        //     }
        // }
    };

    class MenuError : public std::exception
    {
        std::string msg;

    public:
        MenuError(const std::string& msg) : exception(), msg(msg) {}
        virtual ~MenuError() throw() {}
        virtual const char * what() const throw() { return msg.c_str(); }
    };
};

#endif
