#ifndef GFX_H
#define GFX_H

#include <algorithm>
#include <exception>
#include <string>
#include <vector>
#ifdef DEBUG
#include <iostream>
#endif

#ifdef __unix__
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#elif defined(_WIN32)
#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#endif

namespace gfx
{
    class Window
    {
        int m_width;
        int m_height;

        Window() : m_width{640}, m_height{480} {}
        ~Window() = default;
        Window(const Window&) = delete;
        Window& operator=(const Window&) = delete;

    protected:
        static const std::vector<std::pair<int, int>> resolutions;

    public:
        static Window* get_instance() {
            static Window* instance;
            if (!instance)
                instance = new Window;
            return instance;
        }
        int width() const { return m_width; }
        int height() const { return m_height; }
        void resize(const int w, const int h) {
            for (std::pair<int, int> resolution : resolutions)
            {
                if (w + h <= resolution.first + resolution.second)
                {
                    m_width = resolution.first;
                    m_height = resolution.second;
                    break;
                }
            }
        }
    };

    class GameWindow
    {
        int m_width_offset;
        int m_width;  // playable width
        int m_height_offset;
        int m_height;  // playable height

        GameWindow()
            : m_width_offset{0}
            , m_width{Window::get_instance()->width() - m_width_offset}
            , m_height_offset{(Window::get_instance()->height() / 32) * 2}
            , m_height{Window::get_instance()->height() - m_height_offset}
            {}
        ~GameWindow() = default;
        GameWindow(const GameWindow&) = delete;
        GameWindow& operator=(const GameWindow&) = delete;

    public:
        static GameWindow* get_instance() {
            static GameWindow* instance;
            if (!instance)
                instance = new GameWindow;
            return instance;
        }
        int width_offset() const { return m_width_offset; }
        int width() const { return m_width; }
        int height_offset() const { return m_height_offset; }
        int height() const { return m_height; }
        void update_size() {
            m_width_offset = 0;
            m_width = Window::get_instance()->width() - m_width_offset;
            m_height_offset = (Window::get_instance()->height() / 32) * 2;
            m_height = Window::get_instance()->height() - m_height_offset;
        }
    };

    class Tile
    {
        int m_size;

        Tile() : m_size{GameWindow::get_instance()->width() / 32} {}
        ~Tile() = default;
        Tile(const Tile&) = delete;
        Tile& operator=(const Tile&) = delete;

    public:
        static Tile* get_instance() {
            static Tile* instance;
            if (!instance)
                instance = new Tile;
            return instance;
        }
        int size() const { return m_size; }
        void update_size() { m_size = Window::get_instance()->width() / 32; }
    };

    struct Pos
    {
        int x;
        int y;
        bool operator==(const Pos& other) const { return x == other.x && y == other.y; }
    };

    class SDLError : public std::exception
    {
        std::string msg;

    public:
        SDLError() : exception(), msg(SDL_GetError()) {};
        SDLError(const std::string& msg) : exception(), msg(msg) {};
        virtual ~SDLError() throw() {};
        virtual const char* what() const throw() { return msg.c_str(); };
    };

    class SDL
    {
        static constexpr const char *Menu_Font = "./data/font/a-astro-space-font/AstroSpace-eZ2Bg.ttf";

        SDL_Window* m_window;
        SDL_Renderer* m_renderer;
        TTF_Font* m_font;
        std::pair<int, int> m_window_size;

    public:
        SDL(Uint32 flags = 0);
        virtual ~SDL();
        void set_draw_color(const Uint8 r, const Uint8 g, const Uint8 b, const Uint8 a);
        void draw_background();
        void draw_header();
        void draw_tile(int x, int y, int w, int h);
        void render();
        int window_width();
        int window_height();
        void draw_box(const SDL_Rect rect, const int line_width);
        void draw_font(const std::string text, const SDL_Rect rect);
        void fill_box(const SDL_Rect rect);
        void draw_opaque_layer(const int w, const int h);
        void reset_window_size() { get_window_size(); }
        std::pair<int, int> get_window_size();
        std::pair<int, int> get_position();
        void set_window_size(const int w, const int h);
    };
}

#endif
